package net.tngou.db.netty.http;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Media Types http请求Content-Type
 * @author tngou@tngou.net
 *
 */
public class ContentType {
	private static Map<String, String> map = new HashMap<String , String>(){
	   {  
	    	//text
		 put("css", "text/css;charset=utf-8");
		 put("js", "text/js;charset=utf-8"); 
	     put("csv", "text/csv;charset=utf-8");  
	     put("html", "text/html;charset=utf-8");
	     put("htm", "text/html;charset=utf-8");  
	     put("xml", "text/xml;charset=utf-8"); 
	     //application
	     put("json", "application/json;charset=utf-8");  
	     put("pdf", "application/pdf");
	     put("ppt", "application/x-ppt");
	     put("xls", "application/-excel");
	     put("doc", "application/msword");
	     put("docx", "application/msword");
	     //img
	     put("png", "image/png");
	     put("jpeg", "image/jpeg");
	     put("jpg", "image/jpg");
	     put("gif", "image/gif");
	     
	     //avi
	     put("mp3", "audio/mp3");
	     put("mp4", "video/mp4");
	     put("mp4", "video/mp4"); 
	     put("awf", "application/vnd.adobe.workflow");
	 }};  
	 
	public static String getType(String uri)
	{
		Pattern pattern = Pattern.compile("\\S*[?]\\S*");
		Matcher matcher = pattern.matcher(uri);  
		  
        String[] spUrl = uri.toString().split("/");  
        int len = spUrl.length;  
        String key =len>1? spUrl[len - 1]:"";   
        if(matcher.find()) {  
            String[] spEndUrl = key.split("\\?");  
            String[] sp = spEndUrl[0].split("\\.");
            if(sp.length>1)key=spEndUrl[0].split("\\.")[1];  
        }  else
        {
        	String[] sp = key.split("\\.") ;
        	if(sp.length>1)key=sp[1];
        }  
        key=key.toLowerCase();
		return map.get(key)==null?"text/html;charset=utf-8":map.get(key);
	}
}