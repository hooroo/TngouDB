package net.tngou.db.manage;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexableField;
import org.apache.lucene.store.FSDirectory;

import net.sf.jsqlparser.JSQLParserException;
import net.sf.jsqlparser.parser.CCJSqlParserManager;
import net.sf.jsqlparser.schema.Table;
import net.sf.jsqlparser.statement.Statement;
import net.sf.jsqlparser.statement.select.PlainSelect;
import net.sf.jsqlparser.statement.select.Select;
import net.tngou.db.lucene.Config;

/**
 * TngouDB管理
 * @author tngou@tngou.net
 *
 */
public class TML {
	private static Config CONFIG=Config.getInstance();
	/**
	 * 显示数据库表schema
	 * @return
	 */
	public List<Map<String,Object>> showTables() {
		List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
		File filePath = new File(CONFIG.getPath());
		if(!filePath.isDirectory())
		{
			filePath.mkdirs();
		}
		File[] files = filePath.listFiles();		
		for (File file : files) {
			if(file.isDirectory())
			{
				String tableName = file.getName();
				File indexPath= new File(CONFIG.getPath(), tableName);
				try {
					FSDirectory directory = FSDirectory.open(Paths.get(indexPath.toURI()));
					IndexReader ireader = DirectoryReader.open(directory);
					int count = ireader.maxDoc();
					File indexpath= new File(indexPath.toURI());
					File[] listfsd = indexpath.listFiles();
					long size=0;
					for (File filefsd : listfsd) {
						size=size+directory.fileLength(filefsd.getName());
					}
					long sizefile=size;
					Map<String, Object> map= new HashMap<String , Object>(){{  
							 put("name", tableName);
							 put("count", count);
							 put("size", sizefile);
							 put("iconCls", "icon-table");
						   }};
					list.add(map);
				} catch (IOException e) {
					Map<String, Object> map= new HashMap<String , Object>(){{  
						 put("name", tableName);
						 put("count", 0);
						 put("size", 0);
						 put("iconCls", "icon-table");
					   }};
				     list.add(map);
//					e.printStackTrace();
				}
			}
			
		}
		return list;
		
	}
	
	
	public List<String> descTable(String tableName) {
		List<String> list = new ArrayList<>();
		File indexPath= new File(CONFIG.getPath(), tableName);
		if(!indexPath.isDirectory())indexPath.mkdirs();
		try {
			FSDirectory directory = FSDirectory.open(Paths.get(indexPath.toURI()));
			IndexReader ireader = DirectoryReader.open(directory);
			Document doc =ireader.document(1);
			List<IndexableField> fields = doc.getFields();
			fields.forEach(e->{
				list.add(e.name());
			});
		} catch (IOException e) {
			return  new ArrayList<>();
		}
		return list;
	}
	public static void main(String[] args) {
		
		List<String> list = new TML().descTable("news");
	    list.forEach(e->{System.err.println(e);});
	}
}
