package net.tngou.db.entity;


/**
 * 
* @ClassName: Limit
* @Description:  取值范围
* @author tngou@tngou.net (www.tngou.net)
* @date 2015年5月20日 下午2:45:39
*
 */
public class Limit {

	private int start=0;//开始
	private int size=20;//结束
	public int getStart() {
		return start;
	}
	public void setStart(int start) {
		this.start = start;
	}
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
	
	
	@Override
	public String toString() {
		String s="start:"+start+"\n"
				+"size:"+size+"\n";
		return s;
	}
}
