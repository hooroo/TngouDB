package net.tngou.db.util;

import java.lang.management.ClassLoadingMXBean;
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;
import java.lang.management.MemoryPoolMXBean;
import java.lang.management.MemoryUsage;
import java.lang.management.ThreadMXBean;
import java.util.List;

import org.apache.commons.lang3.SystemUtils;

import com.sun.management.OperatingSystemMXBean;



/**
 * JRE基础信息
 * @author tngou@tngou.net
 *
 */
public class JavaVM {
	
	 //class 文件加载信息
	 private long loaded_class_count;//当前class加载数
	 private long total_loaded_class_count;//开始执行到目前已经加载的类的总数。
	 private long un_loaded_class_count;//Java 虚拟机开始执行到目前已经卸载的类的总数
	 
	 //JVM信息
	 private String jvm_info;//jvm 信息
	 private String jvm_name;//jvm名称
	 private String jvm_vendor;//jvm特供者
	 private String jvm_version;//jvm版本
	 private String jvm_specification_name;//jvm详细名称
	 private String jvm_specification_vendor;//jvm详细提供者
	 private String jvm_specification_version;//jvm详细版本
	 
	 //系统堆栈信息
	 private MemoryUsage heap_memoryUsage;//返回用于对象分配的堆的当前内存使用量。
	 private MemoryUsage non_memoryUsage;// 返回 Java 虚拟机使用的非堆内存的当前内存使用量。
//	 private  List<MemoryPoolMXBean> memoryPoolMXBeans;//内存池表示由 Java 虚拟机管理的内存资源

	 //操作系统信息
	 private String os_name; //操作系统的名称
	 private String os_arch;//体系结构
	 private String os_version;//操作系统的版本
	 private int available_processors;//返回 Java 虚拟机可以使用的处理器数目
	 private long total_physical_memory_size;//系统总的物理内存
	 private long free_physical_memory_size;//剩余系统物理内存
	 private long total_swap_space_size;//交换空间总量
	 private long free_swap_space_size;//可用交换空间
	 private long committed_virtual_memory_size;//分配虚拟机内存
	 
	 //线程信息
	 private int daemon_thread_count; //Java 虚拟机启动或峰值重置以来峰值活动线程计数。
	 private int peak_thread_count;//返回自从 Java 虚拟机启动或峰值重置以来峰值活动线程计数。
	 private long total_started_thread_count;// Java 虚拟机启动以来创建和启动的线程总数目
	 private int thread_count;//返回活动线程的当前数目，包括守护线程和非守护线程。
	 private long current_thread_cpu_time;//回当前线程的总 CPU 时间（以毫微秒为单位）。
	 private long current_thread_user_time;//返回当前线程在用户模式中执行的 CPU 时间（以毫微秒为单位）。
//	 private java.lang.management.ThreadInfo[] threadInfos;// 返回所有活动线程的线程信息，并带有堆栈跟踪和同步信息。
	
	//构造
	public JavaVM() {
		ClassLoadingMXBean bean = ManagementFactory.getClassLoadingMXBean();
		this.loaded_class_count=bean.getLoadedClassCount();
		this.total_loaded_class_count = bean.getTotalLoadedClassCount();
		this.un_loaded_class_count = bean.getUnloadedClassCount();
		this.jvm_info = SystemUtils.JAVA_VM_INFO;
		this.jvm_name = SystemUtils.JAVA_VM_NAME;
		this.jvm_vendor = SystemUtils.JAVA_VM_VENDOR;
		this.jvm_version = SystemUtils.JAVA_VM_VERSION;
		this.jvm_specification_name = SystemUtils.JAVA_VM_SPECIFICATION_NAME;
		this.jvm_specification_vendor = SystemUtils.JAVA_VM_SPECIFICATION_VENDOR;
		this.jvm_specification_version = SystemUtils.JAVA_VM_SPECIFICATION_VERSION;
		MemoryMXBean mbean = ManagementFactory.getMemoryMXBean();
		this.heap_memoryUsage = mbean.getHeapMemoryUsage();
		this.non_memoryUsage = mbean.getNonHeapMemoryUsage();
//		this.memoryPoolMXBeans=ManagementFactory.getMemoryPoolMXBeans();
		this.os_arch = SystemUtils.OS_ARCH;
		this.os_name = SystemUtils.OS_NAME;
		this.os_version = SystemUtils.OS_VERSION;
		this.available_processors=ManagementFactory.getOperatingSystemMXBean().getAvailableProcessors();
		OperatingSystemMXBean opbean=(OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean();
		this.committed_virtual_memory_size = opbean.getCommittedVirtualMemorySize();
		this.free_physical_memory_size = opbean.getFreePhysicalMemorySize();
		this.free_swap_space_size = opbean.getFreeSwapSpaceSize();
		this.total_physical_memory_size = opbean.getTotalPhysicalMemorySize();
		this.total_swap_space_size = opbean.getTotalSwapSpaceSize();
		ThreadMXBean thbean = ManagementFactory.getThreadMXBean();
		this.daemon_thread_count = thbean.getDaemonThreadCount();
		this.total_started_thread_count = thbean.getTotalStartedThreadCount();
		this.thread_count = thbean.getThreadCount();
		this.peak_thread_count = thbean.getPeakThreadCount();
		this.current_thread_cpu_time = thbean.getCurrentThreadCpuTime();
		this.current_thread_user_time = thbean.getCurrentThreadUserTime();
//		this.threadInfos = thbean.dumpAllThreads(true, true);
		
	}

	public long getLoaded_class_count() {
		return loaded_class_count;
	}

	public void setLoaded_class_count(long loaded_class_count) {
		this.loaded_class_count = loaded_class_count;
	}

	public long getTotal_loaded_class_count() {
		return total_loaded_class_count;
	}

	public void setTotal_loaded_class_count(long total_loaded_class_count) {
		this.total_loaded_class_count = total_loaded_class_count;
	}

	public long getUn_loaded_class_count() {
		return un_loaded_class_count;
	}

	public void setUn_loaded_class_count(long un_loaded_class_count) {
		this.un_loaded_class_count = un_loaded_class_count;
	}

	public String getJvm_info() {
		return jvm_info;
	}

	public void setJvm_info(String jvm_info) {
		this.jvm_info = jvm_info;
	}

	public String getJvm_name() {
		return jvm_name;
	}

	public void setJvm_name(String jvm_name) {
		this.jvm_name = jvm_name;
	}

	public String getJvm_vendor() {
		return jvm_vendor;
	}

	public void setJvm_vendor(String jvm_vendor) {
		this.jvm_vendor = jvm_vendor;
	}

	public String getJvm_version() {
		return jvm_version;
	}

	public void setJvm_version(String jvm_version) {
		this.jvm_version = jvm_version;
	}

	public String getJvm_specification_name() {
		return jvm_specification_name;
	}

	public void setJvm_specification_name(String jvm_specification_name) {
		this.jvm_specification_name = jvm_specification_name;
	}

	public String getJvm_specification_vendor() {
		return jvm_specification_vendor;
	}

	public void setJvm_specification_vendor(String jvm_specification_vendor) {
		this.jvm_specification_vendor = jvm_specification_vendor;
	}

	public String getJvm_specification_version() {
		return jvm_specification_version;
	}

	public void setJvm_specification_version(String jvm_specification_version) {
		this.jvm_specification_version = jvm_specification_version;
	}

	public MemoryUsage getHeap_memoryUsage() {
		return heap_memoryUsage;
	}

	public void setHeap_memoryUsage(MemoryUsage heap_memoryUsage) {
		this.heap_memoryUsage = heap_memoryUsage;
	}

	public MemoryUsage getNon_memoryUsage() {
		return non_memoryUsage;
	}

	public void setNon_memoryUsage(MemoryUsage non_memoryUsage) {
		this.non_memoryUsage = non_memoryUsage;
	}

//	public List<MemoryPoolMXBean> getMemoryPoolMXBeans() {
//		return memoryPoolMXBeans;
//	}
//
//	public void setMemoryPoolMXBeans(List<MemoryPoolMXBean> memoryPoolMXBeans) {
//		this.memoryPoolMXBeans = memoryPoolMXBeans;
//	}

	public String getOs_name() {
		return os_name;
	}

	public void setOs_name(String os_name) {
		this.os_name = os_name;
	}

	public String getOs_arch() {
		return os_arch;
	}

	public void setOs_arch(String os_arch) {
		this.os_arch = os_arch;
	}

	public String getOs_version() {
		return os_version;
	}

	public void setOs_version(String os_version) {
		this.os_version = os_version;
	}

	public int getAvailable_processors() {
		return available_processors;
	}

	public void setAvailable_processors(int available_processors) {
		this.available_processors = available_processors;
	}

	public long getTotal_physical_memory_size() {
		return total_physical_memory_size;
	}

	public void setTotal_physical_memory_size(long total_physical_memory_size) {
		this.total_physical_memory_size = total_physical_memory_size;
	}

	public long getFree_physical_memory_size() {
		return free_physical_memory_size;
	}

	public void setFree_physical_memory_size(long free_physical_memory_size) {
		this.free_physical_memory_size = free_physical_memory_size;
	}

	public long getTotal_swap_space_size() {
		return total_swap_space_size;
	}

	public void setTotal_swap_space_size(long total_swap_space_size) {
		this.total_swap_space_size = total_swap_space_size;
	}

	public long getFree_swap_space_size() {
		return free_swap_space_size;
	}

	public void setFree_swap_space_size(long free_swap_space_size) {
		this.free_swap_space_size = free_swap_space_size;
	}

	public long getCommitted_virtual_memory_size() {
		return committed_virtual_memory_size;
	}

	public void setCommitted_virtual_memory_size(long committed_virtual_memory_size) {
		this.committed_virtual_memory_size = committed_virtual_memory_size;
	}

	public int getDaemon_thread_count() {
		return daemon_thread_count;
	}

	public void setDaemon_thread_count(int daemon_thread_count) {
		this.daemon_thread_count = daemon_thread_count;
	}

	public int getPeak_thread_count() {
		return peak_thread_count;
	}

	public void setPeak_thread_count(int peak_thread_count) {
		this.peak_thread_count = peak_thread_count;
	}

	public long getTotal_started_thread_count() {
		return total_started_thread_count;
	}

	public void setTotal_started_thread_count(long total_started_thread_count) {
		this.total_started_thread_count = total_started_thread_count;
	}

	public int getThread_count() {
		return thread_count;
	}

	public void setThread_count(int thread_count) {
		this.thread_count = thread_count;
	}

	public long getCurrent_thread_cpu_time() {
		return current_thread_cpu_time;
	}

	public void setCurrent_thread_cpu_time(long current_thread_cpu_time) {
		this.current_thread_cpu_time = current_thread_cpu_time;
	}

	public long getCurrent_thread_user_time() {
		return current_thread_user_time;
	}

	public void setCurrent_thread_user_time(long current_thread_user_time) {
		this.current_thread_user_time = current_thread_user_time;
	}

//	public java.lang.management.ThreadInfo[] getThreadInfos() {
//		return threadInfos;
//	}
//
//	public void setThreadInfos(java.lang.management.ThreadInfo[] threadInfos) {
//		this.threadInfos = threadInfos;
//	}
	 
}
